/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.giuffre;

/**
 *
 * @author gasdia
 */
public class Main {

    static {
        String archDataModel = System.getProperty("sun.arch.data.model");
        System.out.println("arch : " + archDataModel);
        try {
            System.load("C:\\Dropbox-casa\\Dropbox\\jni\\TestJniNative\\dist\\libTestJniNative.dll");
        } catch (UnsatisfiedLinkError ex) {
            System.out.println("errore!");
        }
    }

    public Main() {

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Main().nativePrint();
    }

    public native void nativePrint();

}
